'use strict';

import mongoose from 'mongoose';

var Slide = new mongoose.Schema({
  content: String,
  active: {
    type: Boolean,
    default: false
  }
});

var PresentationSchema = new mongoose.Schema({
  title: String,
  style: {
    background: String,
    color: String,
    fontSize: Number,
    backgroundUrl: String
  },
  slides: [Slide]
});

export default mongoose.model('Presentation', PresentationSchema);
