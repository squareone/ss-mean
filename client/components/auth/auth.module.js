'use strict';

angular.module('presentorApp.auth', [
  'presentorApp.constants',
  'presentorApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
