'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
    'title': 'Home',
    'state': 'main',
    public: true
  },
  {
    'title': 'Presentations',
    'state': 'presentation'
  }];

  isCollapsed = true;
  //end-non-standard

  constructor(Auth, $state) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
    this.hideNavbar = $state.current.hideNavbar;
  }
}

angular.module('presentorApp')
  .controller('NavbarController', NavbarController);
