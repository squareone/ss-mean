'use strict';
(function(){

class ViewComponent {
  constructor($http, $stateParams, $filter, socket, $rootScope) {
    this.$http = $http;
    this.$stateParams = $stateParams;
    this.presentation = {};
    this.slide = {};
    this.$filter = $filter;
    this.socket = socket;
    this.presentations = [];
    $rootScope.hideMenu = true;
    this.style = {};
  }

  $onInit() {
    this.$http.get('/api/presentations/')
    .then(response => {
      this.presentations = response.data;
      this.socket.syncUpdates('presentation', this.presentations);
      this.presentation = this.$filter('filter')(this.presentations, {_id: this.$stateParams.id})[0];
      var activeSlides = this.$filter('filter')(this.presentation.slides, {active: true});
      this.slide = activeSlides[0] || this.presentation.slides[0];
      this.style = {
        'background' : this.presentation.style.background,
        'color': this.presentation.style.color,
        'font-size': this.presentation.style.fontSize,
      };
      if (this.presentation.style.backgroundUrl) {
        this.style['background-image'] = 'url("'+this.presentation.style.backgroundUrl+'")';
      }
    });
  }
}

angular.module('presentorApp')
  .component('view', {
    templateUrl: 'app/presentation/view/view.html',
    controller: ViewComponent
  });
})();
