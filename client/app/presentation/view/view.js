'use strict';

angular.module('presentorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('view', {
        url: '/presentation/view/:id',
        template: '<view></view>',
        hideNavbar: true
      });
  });
