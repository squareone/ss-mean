'use strict';
(function(){

class PresentationController {

  constructor($http, $scope, socket, Auth, $uibModal) {
    this.$http = $http;
    this.socket = socket;
    this.presentations = [];
    this.addShow = false;
    this.isAdmin = Auth.isAdmin;
    this.modal = $uibModal;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('presentation');
    });
  }

  $onInit() {
    this.$http.get('/api/presentations').then(response => {
      this.presentations = response.data;
      this.socket.syncUpdates('presentation', this.presentations);
    });
  }

  openModal() {
    this.modal.open({
     animation: true,
     templateUrl: 'app/presentation/modal.html',
     controller: 'ModalInstanceCtrl',
     size: 'lg'
   });
  }

  toggleAddForm(){
    this.addShow = !this.addShow;
  }


  deletePresentation(presentation) {
    this.$http.delete('/api/presentations/' + presentation._id);
  }
}

angular.module('presentorApp')
  .component('presentation', {
    templateUrl: 'app/presentation/presentation.html',
    controller: PresentationController
  })
  .controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, $http) {

  $scope.presentation = {
    style: {
      background: '#000000',
      color: '#ffff00',
      fontSize: 40
    }
  };
  $scope.$http = $http;

  $scope.addPresentation = function(presentation) {
      var slides = [];
      var contents = presentation.content.split('\n\n');
      angular.forEach(contents, function(ct){
        slides.push({content: ct});
      });
      console.log(presentation.style);
      $scope.$http.post('/api/presentations', {
        title: presentation.title,
        slides: slides,
        style: presentation.style
      });
  };

  $scope.ok = function () {
    $scope.addPresentation($scope.presentation);
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

})();
