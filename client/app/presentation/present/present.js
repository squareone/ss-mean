'use strict';

angular.module('presentorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('present', {
        url: '/presentation/present/:id',
        template: '<present></present>',
        authenticate: true
      });
  });
