'use strict';

describe('Component: PresentComponent', function () {

  // load the controller's module
  beforeEach(module('presentorApp'));

  var PresentComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    PresentComponent = $componentController('PresentComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
