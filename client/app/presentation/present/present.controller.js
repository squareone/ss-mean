'use strict';
(function(){

class PresentComponent {
  constructor($http, $stateParams, $filter, socket, $window) {
    this.$http = $http;
    this.$stateParams = $stateParams;
    this.presentation = {};
    this.$filter = $filter;
    this.socket = socket;
    this.presentations = [];
    this.window = $window;
  }

  $onInit() {
    this.$http.get('/api/presentations/')
    .then(response => {
      this.presentations = response.data;
      this.presentation = this.$filter('filter')(this.presentations, {_id: this.$stateParams.id})[0];
      this.socket.syncUpdates('presentation', this.presentations);
    });
  }

  select(slide){
    // reset all active slides
    var activeSlides = this.$filter('filter')(this.presentation.slides, {active: true});
    for (var i = 0; i < activeSlides.length; i++) {
      activeSlides[i].active = false;
    }
    slide.active = true;
    this.$http.put('/api/presentations/' + this.$stateParams.id, {
    slides: this.presentation.slides
    }).then();
  }

  openWindow(){
    this.window.open('/presentation/view/'+this.$stateParams.id);
  }
}

angular.module('presentorApp')
  .component('present', {
    templateUrl: 'app/presentation/present/present.html',
    controller: PresentComponent
  });

})();
