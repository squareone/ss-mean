'use strict';

angular.module('presentorApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('presentation', {
        url: '/presentation',
        template: '<presentation></presentation>',
        authenticate: true
      });
  });
