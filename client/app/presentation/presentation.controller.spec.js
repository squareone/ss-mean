'use strict';

describe('Component: PresentationComponent', function () {

  // load the controller's module
  beforeEach(module('presentorApp'));

  var PresentationComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    PresentationComponent = $componentController('PresentationComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
