'use strict';

angular.module('presentorApp.admin', [
  'presentorApp.auth',
  'ui.router'
]);
