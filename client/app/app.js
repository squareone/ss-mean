'use strict';

angular.module('presentorApp', [
  'presentorApp.auth',
  'presentorApp.admin',
  'presentorApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'validation.match',
  'monospaced.elastic',
  'angularScreenfull',
  '720kb.tooltips',
  'colorpicker.module'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
